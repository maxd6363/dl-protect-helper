console.log("DL-Protect helper loaded");

const maxRetry = 100;
const deltaRetry = 100;

let dontfoidRetryCount = 0;
let submitRetryCount = 0;

const dontfoidRetry = setInterval(() => {
    const dontfoid = document.getElementById("dontfoid");
    if (dontfoid) {
        dontfoid.remove();
        clearInterval(dontfoidRetry);
    }
    dontfoidRetryCount++;
    if (dontfoidRetryCount > maxRetry)
        clearInterval(dontfoidRetry);

}, deltaRetry);


const submitRetry = setInterval(() => {
    const subButton = document.getElementById("subButton");

    if (subButton && subButton.innerText == "Continuer") {
        subButton.click();
        clearInterval(submitRetry);
    }
    submitRetryCount++;
    if (submitRetryCount > maxRetry)
        clearInterval(submitRetry);

}, deltaRetry);

const downloadLink = setInterval(() => {
    const urls = document.getElementsByClassName("urls")[0];
    if (urls) {
        const link = urls.getElementsByTagName("a")[0].href;

        // we copy the link to the clipboard
        const input = document.createElement('input');
        input.setAttribute('value', link);
        document.body.appendChild(input);
        input.select();
        document.execCommand('copy');
        document.body.removeChild(input);
        window.location.href = link;
                
        clearInterval(downloadLink);
    }
}, deltaRetry);