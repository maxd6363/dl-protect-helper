console.log("Wawacity helper loaded");

const maxRetry = 100;
const deltaRetry = 100;

let dontfoidRetryCount = 0;
let submitRetryCount = 0;

const dontfoidRetry = setInterval(() => {
    const dontfoid = document.getElementById("dontfoid");
    if (dontfoid) {
        console.log("Removing dontfoid");
        dontfoid.remove();
    }
    dontfoidRetryCount++;
    if (dontfoidRetryCount > maxRetry)
        clearInterval(dontfoidRetry);

}, deltaRetry);


const urlParams = new URLSearchParams(window.location.search);
const id = urlParams.get('id');
let downloadLinkIndex = 0;

if(id) {
   // we search all <td> with the text 1fichier inside
    const tds = document.querySelectorAll('td');
    for (let i = 0; i < tds.length; i++) {
        const td = tds[i];
        if (td.innerText === "1fichier") {
            const parent = td.parentElement;
            const link = parent.querySelector('a')?.href;
            if (link) {
                downloadLinkIndex++;
                //we open in a new tab in the background
                //we wait 0.1 second before continuing
                setTimeout(() => {
                    window.open(link, '_blank');
                    console.log("Opening link :", link);
                }, 100 * downloadLinkIndex);
            }
        }
    }
   
}
